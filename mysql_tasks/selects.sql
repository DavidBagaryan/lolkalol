1.

SELECT d.name  AS name,
       v.total AS `Total number of visitors`
FROM `dinosaurs` AS d
         LEFT JOIN `visitors` AS v ON d.dinosaur_Id = v.dinosaur_id
ORDER BY `Total number of visitors` DESC;

------------------------------------------------------------------------------------------

2.

SELECT d.name                                             AS name,
       GROUP_CONCAT(d.dinosaur_Id ORDER BY d.dinosaur_Id) AS `dinosaur ids`,
       SUM(v.total)                                       AS `Total number of visitors`
FROM `dinosaurs` AS d
         LEFT JOIN `visitors` AS v ON d.dinosaur_Id = v.dinosaur_id
GROUP BY name
ORDER BY `Total number of visitors` DESC;

------------------------------------------------------------------------------------------

3.

SELECT d.name        AS name,
       d.dinosaur_Id AS dinosaur_id,
       v.total       AS `Total number of visitors`
FROM `dinosaurs` AS d
         LEFT JOIN `visitors` AS v ON d.dinosaur_Id = v.dinosaur_id

-- WHERE statement could be replaced by HAVING too, but not recommended
WHERE v.total > (SELECT AVG(total) from visitors)
ORDER BY `Total number of visitors` DESC;

------------------------------------------------------------------------------------------

4.

SELECT name, SUM(total) AS `Total number of visitors`
FROM (
         SELECT name, total
         FROM dinosaurs AS d
                  LEFT JOIN visitors v on d.dinosaur_Id = v.dinosaur_id

         UNION ALL

         SELECT name, total
         FROM dinosaurs_island AS d_i
                  LEFT JOIN visitors_island v_i on d_i.dinosaur_Id = v_i.dinosaur_id
     ) AS union_query
GROUP BY name
ORDER BY name;
