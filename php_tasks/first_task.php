<?php

function isNureek(int $num): string
{
    return $num % 3 === 0;
}

function isRetut(int $num): bool
{
    return $num % 4 === 0;
}

function isHanunga(int $num): string
{
    return isNureek($num) && isRetut($num);
}

function isLastOne($i, $range): bool
{
    return $i === $range - 1;
}

$nums = range(1, 100);
$resultOutput = '';

foreach ($nums as $i => $num) {
    $newOne = $num;

    if (isNureek($num)) {
        $newOne = 'Nureek';
    }

    if (isRetut($num)) {
        $newOne = 'Retut';
    }

    if (isHanunga($num)) {
        $newOne = 'Hanunga';
    }

    $resultOutput .= isLastOne($i, count($nums))
        ? $newOne
        : $newOne . ', ';
}

echo $resultOutput;
