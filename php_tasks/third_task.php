<?php

function countSingles(array $digits, bool $toggleOn = true): bool
{
    if ($toggleOn) {
        return true;
    }

    return count($digits) > 1;
}

function isDividedBy3(int $num): bool
{
    // I had a doubt about the first one - 0, but it can be divided by 3
    // Add a condition to cross it out of range will cost nothing =)
    return $num % 3 === 0;
}

function sumIsLessThan17(array $digits): bool
{
    return array_sum($digits) <= 17;
}

// make this bellow to false to prevent counting the single digits
$countSingles = true;
$nums = range(0, 1000);

foreach ($nums as $num) {

    $digits = str_split($num);
    $sum = array_sum($digits);

    if (countSingles($digits, $countSingles)) {

        if (isDividedBy3($num) && sumIsLessThan17($digits)) {
            echo $num . "\n";
        }

        continue;
    }
}