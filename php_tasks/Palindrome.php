<?php

class Palindrome
{
    /**
     * @var string|null
     */
    private $input;

    /**
     * @var string|null
     */
    private $modifiedInput = null;

    public function __construct(?string $input = null)
    {
        $this->input = $input;
    }

    public function getReport(int $i = null): string
    {
        if ($this->checkInput()) {
            $res = "'{$this->input}' is a palindrome";
        } else {
            $res = "'{$this->input}' is a regular";
        }

        return is_null($i) ? $res : "{$i}. {$res}";
    }

    /**
     * @return bool
     */
    private function checkInput(): bool
    {
        if (!isset($this->input)) {
            throw new UnexpectedValueException('Can not check an empty/nullable input');
        }

        $this->prepareInput();

        return $this->modifiedInput === $this->mbStrReverse($this->modifiedInput);
    }

    private function prepareInput(): void
    {
        $this->modifiedInput = preg_replace("#[[:punct:]]#", "", $this->input);
        $this->modifiedInput = preg_replace("#[[:space:]]#", "", $this->modifiedInput);
        $this->modifiedInput = mb_strtolower($this->modifiedInput);
    }

    private function mbStrReverse(string $str): string
    {
        $reversed = '';
        for ($i = mb_strlen($str); $i >= 0; $i--) {
            $reversed .= mb_substr($str, $i, 1);
        }

        return $reversed;
    }
}

$testCases = [
    'racecar',
    'a man a plan a canal Panama.',
    'R2D2',
    'Desserts, I stressed!',
    'Red rum, sir,$$ is murder.',
    'This is the$$ final one?$####',
    'Какого ху$#ух? Ого как!'
];

foreach ($testCases as $i => $case) {
    $i++;

    $processed = new Palindrome($case);
    echo $processed->getReport($i);

    echo "\n";
}
