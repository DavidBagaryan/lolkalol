const cars = [
    {model: 'Volvo', order: 0, id: 1},
    {model: 'Jeep', order: 2, id: 1},
    {model: 'BMW', order: 0, id: 1},
    {model: 'Ferrari', order: 3, id: 1},
    {model: 'Tesla', order: 1, id: 1},
    {model: 'Bugatti', order: 0, id: 12},
    {model: 'Maserati', order: 3, id: 1123},
];

function compare(a, b) {
    if (a.order !== 0 && b.order === 0) {
        return -1;
    }

    if (a.order === 0 && b.order !== 0) {
        return 1;
    }

    if (a.order < b.order) {
        return -1;
    }

    if (a.order > b.order) {
        return 1;
    }

    return 0;
}

console.log(cars.sort(compare));
