const arrayOne = [
    {user_id: 12, user_name: 'John'},
    {user_id: 65, user_name: 'Martin'},
    {user_id: 24, user_name: 'Boris'},
    {user_id: 3, user_name: 'Vasily'},
];

const arrayTwo = [
    {car_id: 35, car_model: 'Ford'},
    {car_id: 12, car_model: 'Fiat'},
    {car_id: 43, car_model: 'Ferrari'},
];

function toObject(arr, key) {
    let res = {};

    for (let item of arr) {
        if (!item.hasOwnProperty(key)) {
            alert('wrong key name to assign!');
            return;
        }

        res[item[key]] = item;
    }

    return res;
}

console.log(toObject(arrayTwo, 'car_id'));
console.log(toObject(arrayTwo, 'car_model'));
console.log(toObject(arrayOne, 'user_id'));
console.log(toObject(arrayOne, 'user_name'));

