const array = [
    {name: 'Arthur\'s round table', weight: 46},
    {name: 'Bean buzzle bag', weight: 13},
    {name: 'Magic mirror', weight: 32},
    {name: 'Booze fridge', weight: 29},
    {name: 'Booze fridge2'},
    {name: 'Booze fridge3', weight: '29'}
];

array.totalWeight = function () {
    let result = 0;

    for (let item of this) {

        if (item.weight === undefined || typeof item.weight === 'string') {
            continue;
        }

        result += item.weight;
    }

    return result;
};

const total = array.totalWeight();
console.log(total);